/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(document).ready( function (){
    jQuery("#feedmine-block-contents").hide();
    
    jQuery("#block-feedmine-fm-feedback h2").click( function (){
        jQuery("#feedmine-block-contents").slideToggle();
    });
    
    jQuery("#block-feedmine-fm-feedback .form-submit").after("<button id=hide-me>Cancel</button>");
    
    jQuery("#block-feedmine-fm-feedback button#hide-me").click( function (){
        jQuery("#feedmine-block-contents").hide();
        return false;
    });
});