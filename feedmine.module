<?php
/*
 * @file
 * The Feedmine module provides a Drupal block for users to give
 * feedback.  Their feedback along with available user agent data
 * are posted to the specified Redmine project issue queue using the
 * Redmine REST API.
 *
 * To configure, go to admin/config/feedmine and set up your Redmine
 * API access credentials.
 *
 * For more information on Redmine, please go to:
 * http://www.redmine.org
 *
 * For a more through implementation of the Redmine API:
 * http://drupal.org/project/redmine
 *
 * For information on the Redmine API, visit:
 * http://www.redmine.org/projects/redmine/wiki/Rest_api
 *
 */

// Include additional module files
module_load_include('inc', 'feedmine', 'feedmine_rm');
module_load_include('inc', 'feedmine', 'feedmine.admin');

/**
 * Implementation of hook_help().
 */
function feedmine_help($path, $args) {
  if($path == 'admin/help#feedmine') {
    $help_text = feedmine_admin_overview();
    return $help_text;
  }
}

/**
 * Implements hook_menu() to provide:
 *  Module config on admin settings page
 *  Module config page
 *
 * @return
 * Array contianing menu definition for Feedmine module.
 */
function feedmine_menu() {
  $items = array();

  $items['admin/config/services/feedmine'] = array(
    'title' => 'Feedmine',
    'description' => t('Integrate wih Redmine to collect user feedback.'),
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  $items['admin/config/services/feedmine/feedmine_settings'] = array(
    'title' => 'Feedmine Settings',
    'description' => t('Configure Feedmine settings here.'),
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('feedmine settings'),
    'page callback' => 'feedmine_admin_overview',
    'file' => 'feedmine.admin.inc',
  );
  $items['admin/config/services/feedmine/feedmine_settings/rmapi'] = array(
    'title' => 'Redmine API Config',
    'description' => t('Configure Redmine API settings.'),
    'type' => MENU_LOCAL_TASK,
    'access arguments' => array('feedmine settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('feedmine_admin_rmapi_config'),
    'file' => 'feedmine.admin.inc',
  );
  $items['admin/config/services/feedmine/feedmine_settings/rmproject'] = array(
    'title' => 'Redmine Project Selector',
    'description' => t('Select a Redmine project.'),
    'type' => MENU_LOCAL_TASK,
    'access arguments' => array('feedmine settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('feedmine_rmproject_select'),
    'file' => 'feedmine.admin.inc',
  );

  return $items;
}

/**
 * Implementation of hook_perm().
 *
 * Defines module permissions.
 */
function feedmine_permission() {
  return array(
    'feedmine settings' => array(
      'title' => t('Administer Feedmine'),
      'description' => t('Administer Feedmine settings.'),
    ),
    'feedmine submit feedback' => array(
      'title' => t('Submit Feedback'),
      'description' => t('Submit feedback using Feedmine.'),
    ),
  );
}

/**
 * Implementation of hook_block_info().
 *
 * This hook declares what blocks are provided by the module.
 */
function feedmine_block_info() {
  // A block to let users submit feedback to Redmine.
  $blocks['fm_feedback'] = array(
    'info' => t('Feedmine: Feedback submission'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function feedmine_block_view($delta = '') {
  switch ($delta) {
    case 'fm_feedback':
      // Ensure user has access to view feedback block.
      if(!user_access('feedmine submit feedback')){
        $block = NULL;
        break;
      }
      drupal_add_js(drupal_get_path('module', 'feedmine') . '/js/feedmine.js', 'file');
      drupal_add_css(drupal_get_path('module', 'feedmine') . '/css/feedmine.css', 'file');
      // The subject is displayed at the top of the block.
      $block['subject'] = t('Leave feedback');
      $block['content'] = drupal_get_form('feedmine_block_contents');
    break;
  }
  return $block;
}

/**
 * Generates the feedmine block contents condintionally based on
 * wether the module settings have already been configured.
 */
function feedmine_block_contents($form, &$form_state) {
  // Verify if Feedmine settings have been configured.
  $rm_pid = variable_get('feedmine_rmprojectid', NULL);
  if (!isset($rm_pid)) {
    // Display a link to the feedmine module config page.
    $text = t('Click here to configure Redmine settings and enable Feedmine.');
    $path = 'admin/config/feedmine/feedmine_settings';
    $form['feedmine_not_configured'] = array(
      '#type' => 'item',
      '#title' => 'NOTICE',
      '#markup' => l($text, $path),
    );
  }
  else {
    // Display the form to submit feedback.
    $form['feedmine_tracker'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#description' => t('Type of feedback'),
      '#default_value' => '1',
      '#options' => array(
        '1' => t('Bug'),
        '2' => t('Feature'),
        '3' => t('Support'),
      ),
    );
    $form['feedmine_subject'] = array(
      '#type' => 'textfield',
      '#size' => 20,
      '#title' => t('Subject'),
      '#required' => TRUE,
    );
    $form['feedmine_feedback'] = array(
      '#type' => 'textarea',
      '#cols' => 5,
      '#rows' => 5,
      '#title' => t('Feedback'),
      '#description' => t('Please leave a detailed description.'),
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit')
    );
    $form['#submit'][] = 'feedmine_issue_report';
  }
  // Return the block contents.
  return $form;
}
